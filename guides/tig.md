tig guides
----------

# view

* d - diff
* m - main

# move-1-page

* -
* Space

# move-alf

* Ctrl-U
* Ctrl-D

# scroll

* Ctrl-Y
* Ctrl-E

# display set

* # line-number
* A arthor
* D date
